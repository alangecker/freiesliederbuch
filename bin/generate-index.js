const fs = require('fs')
const path = require('path')

const songsDir = path.join(path.join(__dirname, '../songs'))

function collectFilesInDir(p) {
    const dir = fs.opendirSync(p)
    const res = []
    let f
    while(f = dir.readSync()) {
        if(f.isDirectory()) {
            res.push(...collectFilesInDir(path.join(p, f.name)))
        } else if(f.isFile()) {
            res.push(path.join(p, f.name))
        }
    }
    return res
}

const files = collectFilesInDir(songsDir)
const filesRelative = files.map(s => s.slice(songsDir.length+1))


function findChordProTag(key, source) {
    const match = source.match(new RegExp('\\{'+key+':(.*?)\\}'))
    if(!match) return null

    return match[1].trim()
}

const out = []
for(let file of filesRelative) {
    try {
        const f = fs.statSync(path.join(songsDir, file))
        if(file.match(/\.chordpro$/)) {
            const src = fs.readFileSync(path.join(songsDir, file), 'utf-8')
            const title = findChordProTag('title', src)
            const artist = findChordProTag('artist', src)
            const tags = findChordProTag('tags', src).split(',').map(s => s.trim())
            out.push({
                title: title,
                artist: artist,
                tags: tags,
                type: 'chordpro',
                path: file,
                timeModified: f.mtime,
            })
        } else if(file.match(/\.md$/)) {
            const p = file.split('/')
            out.push({
                title: p[p.length-1].replace(/\.md$/, ''),
                artist: '',
                tags: [],
                type: 'md',
                path: file,
                timeModified: f.mtime,
            })
        }
    } catch(err) {
        console.error('error which parsing file '+file)
        console.error(err)
    }
}
fs.writeFileSync('index.json', JSON.stringify(out), 'utf-8')