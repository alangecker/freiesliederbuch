# Freies Liederbuch
https://freiesliederbuch.de

Alle Songs im [ChordPro Format](https://www.chordpro.org)

Frontend-Code: https://gitlab.com/alangecker/freiesliederbuch-frontend

## Neues Lied hinzufügen
- Erstelle einfach eine Datei mit der Endung `.chordpro` im Ordner `songs`.
- Die Unterordner-Struktur darin ist nur zur Übersichtlichkeit da, die Namen werden darüber hinaus aber für die Webseite ignoriert.
- Wenn du keine Rechte hast das Liederbuch direkt zu ändern, kannst du entweder
    * Das Projekt "forken" (=eine Kopie davon in deinem Profil hier erstellen), die Änderungen dort anwenden und im Anschluss einen sogenannten "Merge Request" erstellen. Wenn es zu Beginn etwas kompliziert erscheint, hol dir gern ein\*e Programmierer\*in deines Vertrauens beiseite :)
    * oder per Mail anschreiben und um die Bearbeitungsrechte fragen: freiesliederbuch@riseup.net (Hinweis: du brauchst einen Account hier auf Gitlab)


## Beispiel-Song im ChordPro-Format
Weitere Erklärungen unter https://www.chordpro.org/chordpro/index.html
```
{title: Dämmerlicht}
{artist: Arbeitstitel Tortenschlacht}
{tags: schwer, deutsch, torte, revolution, aktuell}
{capo: 7}

{start_of_chorus}
[Dm]In Zeiten des Dämmerlichts [Gm]die Integrität bewaren
wenn [Dm]Unrecht zu Recht wird, wird [Am]Richtiges tun illegal
{end_of_chorus}


{start_of_verse}
Zerstörung entgegentreten, Konzernen das Handwerk legen,...
{end_of_verse}


{comment: Chorus}
```

#### Anmerkungen
- Nicht alle "Directives" werden unterstützt. Siehe https://github.com/martijnversluis/ChordSheetJS/#supported-chordpro-directives
- Obwohl nicht aufgeführt ist die Verwendung von `columns` möglich.
- Zusätzlich wurde für das Liederbuch die Direktive `tags` eingeführt.